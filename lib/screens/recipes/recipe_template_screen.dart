import 'package:cookbook/bloc/recipe_bloc.dart';
import 'package:cookbook/models/recipe.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RecipeTemplateScreen extends StatelessWidget {
  RecipeTemplateScreen({Key? key}) : super(key: key);

  final ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecipeBloc, RecipeState>(
      buildWhen: (previous, current) =>
          previous.currentRecipe != current.currentRecipe,
      builder: (context, state) {
        if (state.currentRecipe == null) {
          return Container();
        } else {
          return SingleChildScrollView(
            controller: _scrollController,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: Text(
                    state.currentRecipe!.name,
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: 25),
                  child: Text(
                    'Рецепт приготовления',
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 15),
                  child: Text(
                    'Ингредиенты:',
                    style: Theme.of(context).textTheme.headline3,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 30),
                  child: ListView.separated(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    separatorBuilder: (context, position) => const Padding(
                      padding: EdgeInsets.only(bottom: 10),
                    ),
                    itemCount: state.currentRecipe!.ingredients.length,
                    itemBuilder: (context, index) => Row(
                      children: [
                        Text(
                          ("${index + 1}. "),
                        ),
                        Text(
                          (state.currentRecipe!.ingredients.keys
                                  .elementAt(index) +
                              ' '),
                        ),
                        Text(
                          state.currentRecipe!.ingredients.values
                              .elementAt(index),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 20),
                  child: Text(
                    state.currentRecipe!.description,
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                Container(
                  alignment: Alignment.topCenter,
                  child: Image.asset(
                    (state.currentRecipe!.photo ?? '/assets/ramen.png'),
                    width: 200,
                    height: 200,
                  ),
                ),
              ],
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
            ),
          );
        }
      },
    );
  }
}
