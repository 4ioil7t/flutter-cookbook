import 'package:cookbook/bloc/recipe_bloc.dart';
import 'package:cookbook/models/recipe.dart';
import 'package:cookbook/screens/recipes/recipe_template_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RecipesScreen extends StatefulWidget {
  const RecipesScreen({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  _RecipesScreenPageState createState() => _RecipesScreenPageState();
}

class _RecipesScreenPageState extends State<RecipesScreen> {
  bool _isTablet = false;
  late Recipe currRecipes;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RecipeBloc, RecipeState>(
      buildWhen: (previous, current) => previous != current,
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            title: Text(widget.title),
          ),
          body: OrientationBuilder(
            builder: (context, orientation) {
              if (MediaQuery.of(context).size.width > 600) {
                _isTablet = true;
              } else {
                _isTablet = false;
              }
              return SafeArea(
                minimum: const EdgeInsets.only(top: 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      child: ListView.separated(
                        separatorBuilder: (context, position) => const Padding(
                          padding: EdgeInsets.only(bottom: 20),
                        ),
                        itemCount: state.recipes.length,
                        itemBuilder: (context, index) => GestureDetector(
                          onTap: () {
                            context.read<RecipeBloc>().add(RecipeSelected(
                                currentRecipe: state.recipes[index]));
                            if (!_isTablet) {
                              Route myRoute = MaterialPageRoute(
                                builder: (context) => Scaffold(
                                  appBar: AppBar(
                                    title: Text('Рецепт блюда: ' +
                                        state.recipes[index].name),
                                  ),
                                  body: SafeArea(
                                    minimum: const EdgeInsets.fromLTRB(
                                        15, 20, 15, 0),
                                    child: RecipeTemplateScreen(),
                                  ),
                                ),
                              );
                              Navigator.push(context, myRoute);
                            }
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Expanded(
                                child: Container(
                                  padding: const EdgeInsets.all(12),
                                  child: Text(
                                    state.recipes[index].name,
                                    style: TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.normal,
                                      color: (state.recipes[index].name ==
                                              (state.currentRecipe?.name))
                                          ? Theme.of(context)
                                              .scaffoldBackgroundColor
                                          : Colors.black,
                                    ),
                                  ),
                                  decoration: BoxDecoration(
                                    color: (state.recipes[index].name ==
                                            (state.currentRecipe?.name))
                                        ? Colors.blue
                                        : Theme.of(context)
                                            .scaffoldBackgroundColor,
                                  ),
                                ),
                              ),
                              const Icon(Icons.arrow_right)
                            ],
                          ),
                        ),
                      ),
                    ),
                    _isTablet
                        ? Expanded(
                            child: SafeArea(
                              minimum:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: RecipeTemplateScreen(),
                            ),
                          )
                        : Container(),
                  ],
                ),
              );
            },
          ),
        );
      },
    );
  }
}
