part of 'recipe_bloc.dart';

@immutable
abstract class RecipeEvent {}

class RecipesLoaded extends RecipeEvent {}

class RecipeSelected extends RecipeEvent {
  RecipeSelected({required this.currentRecipe});
  final Recipe currentRecipe;
}
