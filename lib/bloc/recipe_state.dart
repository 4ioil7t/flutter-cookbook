part of 'recipe_bloc.dart';

class RecipeState extends Equatable {
  const RecipeState({required this.recipes, this.currentRecipe});
  final List<Recipe> recipes;
  final Recipe? currentRecipe;

  @override
  List<Object?> get props => [recipes, currentRecipe];
}
