import 'package:bloc/bloc.dart';
import 'package:cookbook/models/recipe.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'recipe_event.dart';
part 'recipe_state.dart';

class RecipeBloc extends Bloc<RecipeEvent, RecipeState> {
  RecipeBloc() : super(const RecipeState(recipes: <Recipe>[])) {
    on<RecipesLoaded>(
      (event, emit) {
        emit(
          const RecipeState(
            recipes: <Recipe>[
              Recipe(
                name: 'Шаурма',
                ingredients: <String, String>{
                  'Лаваш': '1 шт',
                  'Курица': '200 гр',
                  'Овощи': '300 гр',
                  'Майонез': '100 гр',
                },
                description:
                    'На лавашик положить обжаренную на вертеле курочку и мелко нарезанные овощи (по своему усмотрению, шаурма - произведение искусства, каждый художник видит по-разному), майонезик. Хоба-хоба - заворачиваем. Кушаем.',
                photo: 'assets/shawarma.png',
              ),
              Recipe(
                  name: 'Рамен',
                  ingredients: <String, String>{
                    'Свинина': '700 гр',
                    'Лапша': '400 - 500 гр',
                    'Соевый соус': '100 гр',
                    'Имбирь': '5 - 10гр',
                    'Яйцо': '2 шт',
                  },
                  description:
                      'Порезать и отварить свинину, сделать бульон. Через 30 минут перелить бульон в другую кастрюлю, мясо вытаскивать не нужно. В первую кастрюлю добавить корицу/имбирь. Вскипятить, добавить сахар и соевый соус, затем варить 4 часа. Приготовить лапшу (5 минут) и разложить по тарелкам. Налить в тарелку бульон и переложить нарезанное мясо. Добавить сваренное всмятку яйцо. Украсить зеленью (лучше зеленым луком).',
                  photo: 'assets/ramen.png')
            ],
          ),
        );
      },
    );
    on<RecipeSelected>((event, emit) {
      emit(RecipeState(
          recipes: state.recipes, currentRecipe: event.currentRecipe));
    });
  }
}
