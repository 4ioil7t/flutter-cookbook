import 'package:equatable/equatable.dart';

class Recipe extends Equatable {
  const Recipe({
    required this.name,
    required this.ingredients,
    required this.description,
    this.photo,
  });

  final String name;
  final Map<String, String> ingredients;
  final String description;
  final String? photo;

  @override
  List<Object?> get props => [name, ingredients, description, photo];

  @override
  String toString() {
    return '{name: $name, ingredients: $ingredients, description: $description}';
  }
}
