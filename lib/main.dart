import 'package:cookbook/bloc/recipe_bloc.dart';
import 'package:flutter/material.dart';

import 'package:cookbook/screens/screens.barrel.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<RecipeBloc>(
      create: (context) => RecipeBloc()..add(RecipesLoaded()),
      child: _AppView(),
    );
  }
}

class _AppView extends StatefulWidget {
  @override
  _AppViewState createState() => _AppViewState();
}

class _AppViewState extends State<_AppView> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        textTheme: const TextTheme(
          headline1: TextStyle(
              fontSize: 38, fontWeight: FontWeight.normal, color: Colors.black),
          headline2: TextStyle(
              fontSize: 22, fontWeight: FontWeight.normal, color: Colors.black),
          headline3: TextStyle(
              fontSize: 16, fontWeight: FontWeight.w500, color: Colors.black),
        ),
      ),
      home: const RecipesScreen(title: 'Поваренная книга на Flutter'),
    );
  }
}
